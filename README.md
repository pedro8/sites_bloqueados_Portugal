# Sites Bloqueados em Portugal
O poder é dos cidadões e dos tribunais

# Início do bloqueio
> No dia 30 de Agosto de 2015 foi assinado um [memorando de entendimento](https://sitesbloqueados.pt/memorando-de-entendimento/) entre o IGAC e o MAPiNET que permite, entre outros, bloquear sites de uma forma única e exclusivamente administrativa. De início, o MAPiNET sempre que bloqueava sites, divulgava a lista. Agora, a transparência ainda é menor, e não é divulgada qualquer lista. Para além do IGAC/MAPiNET, também o SRIJ tem a possibilidade de bloquear sites, de acordo com o Decreto-Lei nº 66/2015 de 29 Abril. Estes bloqueios são requeridos por entidades administrativas **e não necessitam de passar por um tribunal**.

> Cada vez que um site é bloqueado, é [...] possível detectar esse bloqueio [...]. [Fazemos] [...] o serviço [...] que as entidades que [...] [bloqueiam] sites deveriam fazer. Esta é a lista actualizada de todos os sites que estão bloqueados em Portugal. Neste momento contabilizamos **634** sites nesta lista.

Fonte: [AHOY! SITES BLOQUEADOS](https://sitesbloqueados.pt/)


# Solução universal
Dado que por razões profissionais ou de segurança [tenho de não usar os DNS portugueses](https://www.reddit.com/r/portugal/comments/3q0q9x/aceder_aos_sites_bloqueados/) ocasionalmente, mas desejando eu cumprir um memorando de entendimento entre uma entidade administrativa e outras entidades com os interesses das empresas promotoras da cultura, sem as quais esta não existiria, extraí a lista do https://sitesbloqueados.pt/ e coloquei-a neste repositório.

## Como usar
* Download do uBlock Origin
* Adicionar o filtro https://raw.githubusercontent.com/pedro7/sites_bloqueados_Portugal/master/mapinet.txt
* Enjoy the Internet, livre de conteúdo ilegal
* AVISO: o uBlock Origin também bloqueia publicidade, tenham isso em conta
